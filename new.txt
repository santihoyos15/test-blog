
This is my seconde file edited with Vim.

I'll tell you some basic commands to use vim.

To edit a file in Vim you use the command vim <file>

To create a new file and edit it you use  vim <new file name>

To edit a file you push i key and enter.

To end you use Esc 

To save the file you use ":" key and enter w

To leave Vim you enter q after using the last command

